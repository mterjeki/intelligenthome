package hu.bme.webeszolg.intelligenthome;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;

@Api(
	name = "intelligentHomeLogger",
	version = "v1" )
public class LoggerAPI {
	
	private static final Logger logger = Logger.getLogger( LoggerAPI.class.getSimpleName() );
	
	@ApiMethod( httpMethod = HttpMethod.POST, path = "/log" )
	public void logger( @Named( "message" ) final String message ) {
		logger.log( Level.INFO, message );
	}
	
}
