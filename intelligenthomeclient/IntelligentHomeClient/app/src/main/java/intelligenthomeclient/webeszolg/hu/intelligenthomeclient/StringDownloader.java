package intelligenthomeclient.webeszolg.hu.intelligenthomeclient;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;

public abstract class StringDownloader extends AsyncTask<String, Void, Object> {
    private static final String TAG                    = StringDownloader.class.getSimpleName();
    public static final  int    REQ_CODE     = 10001;

    private static final int    TIMEOUT          = 8000;
    private static final int    HTTP_ERROR_CODES = 400;
    private static final String CHARACTER_CODE   = "UTF-8";

    private final int              mRequestCode;
    private       String           mErrorMessage;
    private       DownloadListener mListener;

    public StringDownloader(final int requestCode, final Context context,
                            final DownloadListener listener) {
        super();
        mRequestCode = requestCode;
        mErrorMessage = "Error occured";
        mListener = listener;
    }

    @Override
    protected Object doInBackground(final String... params) {
        try {
            Log.d(TAG, "open connection");
            final ArrayList<BasicNameValuePair> headers = getHeaders();
            final HttpURLConnection connection = (HttpURLConnection) new URL(getUrl())
                    .openConnection();

            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            final HttpMethod methodName = getHttpMethodName();
            connection.setRequestMethod(methodName.getName());
            connection.setDoInput(true);


            if (methodName == HttpMethod.POST && headers != null) {
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);
                final OutputStream stream = connection.getOutputStream();
                final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream,
                        CHARACTER_CODE));
                Log.d(TAG, "Setting POST headers " + getJSONPostHeader(headers));
                writer.write(getJSONPostHeader(headers));
                writer.flush();
                writer.close();
                stream.close();
            }

            Log.d(TAG, connection.getResponseCode() + connection.getResponseMessage());
            if (connection.getResponseCode() < HTTP_ERROR_CODES) {
                return processResponse(getContent(connection.getInputStream()));
            }
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(final Object o) {
        if (mListener != null) {
            if (o == null) {
                mListener.onBackendError(mErrorMessage);
            } else {
                mListener.onBackendSuccess(mRequestCode, o);
            }
        }
    }

    private String getJSONPostHeader(final ArrayList<BasicNameValuePair> parameters) {
        final JSONObject json = new JSONObject();
        for (final BasicNameValuePair pair : parameters) {
            try {
                json.put(pair.getName(), pair.getValue());
            } catch (final JSONException e) {
                e.printStackTrace();
            }
        }
        return json.toString();
    }

    private String getNamePostHeader(final ArrayList<BasicNameValuePair> parameters) throws
            UnsupportedEncodingException {
        final StringBuilder result = new StringBuilder();
        boolean first = true;
        for (final BasicNameValuePair pair : parameters) {
            if (first) {
                first = false;
            } else {
                result.append('&');
            }

            result.append(URLEncoder.encode(pair.getName(), CHARACTER_CODE));
            result.append('=');
            result.append(URLEncoder.encode(pair.getValue(), CHARACTER_CODE));
        }

        return result.toString();
    }

    private String getContent(final InputStream is) {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(
                CHARACTER_CODE)));
        final StringBuilder builder = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append('\n');
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        Log.d("Respone: ", builder.toString());
        return builder.toString();

    }

    public abstract HttpMethod getHttpMethodName();

    public abstract ArrayList<BasicNameValuePair> getHeaders();

    public abstract String getUrl();

    public abstract Object processResponse(final String response);

    public enum HttpMethod {
        POST("POST"), GET("GET");
        private final String mName;

        HttpMethod(final String name) {
            mName = name;
        }

        private String getName() {
            return mName;
        }
    }

    public interface DownloadListener {
        void onBackendSuccess(final int requestCode, final Object result);

        void onBackendError(final String errorMessage);
    }
}