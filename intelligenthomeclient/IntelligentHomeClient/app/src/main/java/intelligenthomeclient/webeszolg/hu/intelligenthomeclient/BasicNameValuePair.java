package intelligenthomeclient.webeszolg.hu.intelligenthomeclient;

/**
 * Created by mterjeki on 2015.05.06..
 */
public class BasicNameValuePair {
    private final String mName;
    private final String mValue;

    public BasicNameValuePair(final String name, final String value) {
        this.mName = name;
        this.mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public String getName() {
        return mName;
    }
}
