package intelligenthomeclient.webeszolg.hu.intelligenthomeclient;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by mterjeki on 2015.05.06..
 */
public class Logger extends StringDownloader {

    private String message;

    public Logger(Context context, DownloadListener downloadListener, String message) {
        super(StringDownloader.REQ_CODE, context, downloadListener);

        this.message = message;
    }

    @Override
    public HttpMethod getHttpMethodName() {
        return HttpMethod.POST;
    }

    @Override
    public ArrayList<BasicNameValuePair> getHeaders() {
        final ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("message",message));
        return params;
    }

    @Override
    public String getUrl() {
        try {
            return "https://1-dot-intelligent-home-hw.appspot.com/_ah/api/log?message=" + URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object processResponse(String response) {
        return null;
    }
}
