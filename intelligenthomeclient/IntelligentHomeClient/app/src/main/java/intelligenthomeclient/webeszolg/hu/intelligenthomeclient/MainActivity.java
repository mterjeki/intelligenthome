package intelligenthomeclient.webeszolg.hu.intelligenthomeclient;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import javax.xml.datatype.Duration;


public class MainActivity extends Activity implements StringDownloader.DownloadListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar  );
        seekBar.setMax(50);
        final Switch tv = (Switch) findViewById(R.id.switch3);
        final Switch gaztuzhely = (Switch)findViewById(R.id.switch1);
        final Switch riaszto = (Switch)findViewById(R.id.switch2);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                new Logger(MainActivity.this, MainActivity.this, "Hőmérséklet value is: " + seekBar.getProgress()).execute();
            }
        });

        tv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    new Logger(MainActivity.this, MainActivity.this, "TV is on").execute();
                } else {
                    new Logger(MainActivity.this, MainActivity.this, "TV is off").execute();
                }

            }
        });

        gaztuzhely.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    new Logger(MainActivity.this, MainActivity.this, "Gáztűzhely is on").execute();
                } else {
                    new Logger(MainActivity.this, MainActivity.this, "Gáztűzhely is off").execute();
                }

            }
        });

        riaszto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    new Logger(MainActivity.this, MainActivity.this, "Riasztó is on").execute();
                } else {
                    new Logger(MainActivity.this, MainActivity.this, "Riasztó is off").execute();
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // Inflate the menu; this adds items to the action bar if it is present.
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackendSuccess(int requestCode, Object result) {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackendError(String errorMessage) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT);
    }
    
}
